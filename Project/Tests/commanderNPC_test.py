import random

from Project.battles.commanders_npc import CommanderNPC
from Project.battles.State import State
from Project.battles.CommandersType import CommandersType
from Project.battles.vulnerability import commanders_vulnerability


class TestCommanderNPC:
    commander_name = 'Genghis Khan'
    commander_type = CommandersType.Cavalry_Skill
    max_ap = 100

    def setup_method(self):

        random.seed(99)

    def test_init(self):
        commander_test = CommanderNPC()

        assert commander_test.name == self.__class__.commander_name
        assert commander_test.commanders_type == self.__class__.commander_type
        assert commander_test.vulnerability == commanders_vulnerability[self.__class__.commander_type]
        assert commander_test.AP == self.__class__.max_ap
        assert commander_test.attack_point is None
        assert commander_test.defence_point is None
        assert commander_test.hit_power == 20
        assert commander_test.state == State.READY

    def test_str(self):
        commander_test = CommanderNPC()
        assert str(commander_test) == f'Name: {self.__class__.commander_name}' \
                                      f' Type: {self.__class__.commander_type.name},' \
                                      f' HP: {self.__class__.max_ap}'
