from Project.battles.Commander import Commander
from Project.battles.CommandersType import CommandersType
from Project.battles.State import State
from Project.battles.vulnerability import commanders_vulnerability


class TestCommanderClass:
    commander_name = 'Leonidas'
    commander_type = CommandersType.Infantry_Support
    max_ap = 100

    def test_init(self):
        commander_test = Commander(name=self.__class__.commander_name,
                                   commanders_type=self.__class__.commander_type)

        assert commander_test.name == self.__class__.commander_name
        assert commander_test.commanders_type == self.__class__.commander_type
        assert commander_test.vulnerability == commanders_vulnerability[self.__class__.commander_type]
        assert commander_test.AP == self.__class__.max_ap
        assert commander_test.attack_point is None
        assert commander_test.defence_point is None
        assert commander_test.hit_power == 20
        assert commander_test.state == State.READY

    def test_str(self):
        commander_test = Commander(name=self.__class__.commander_name,
                                   commanders_type=self.__class__.commander_type)
        assert str(commander_test) == f'Name: {self.__class__.commander_name}' \
                                      f' Type: {self.__class__.commander_type.name},' \
                                      f' HP: {self.__class__.max_ap}'
