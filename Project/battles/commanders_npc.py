import random

from Project.battles.CommandersType import CommandersType
from Project.battles.Commander import Commander
from Project.battles.damage import Damage
from Project.battles.ChoiceCommander import ChoiceCommanders


class CommanderNPC(Commander):
    def __init__(self):
        rand_type_value = random.randint(CommandersType.min_value(), CommandersType.max_value())
        rand_commander_type = CommandersType(rand_type_value)
        rand_comm_name = random.choice(list(ChoiceCommanders.get(rand_commander_type, {}).keys()))

        super().__init__(rand_comm_name, rand_commander_type)

    def next_step_points(self, **kwargs):
        attack_point = Damage(random.randint(Damage.min_value(), Damage.max_value()))
        defence_point = Damage(random.randint(Damage.min_value(), Damage.max_value()))
        super().next_step_points(next_attack=attack_point,
                                 next_defence=defence_point)


if __name__ == '__main__':
    comNPC = CommanderNPC()
    comNPC.next_step_points()
    print(comNPC)
