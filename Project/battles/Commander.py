from Project.battles.CommandersType import CommandersType
from Project.battles.vulnerability import commanders_vulnerability
from Project.battles.damage import Damage
from Project.battles.State import State


class Commander:
    def __init__(self, name: str, commanders_type: CommandersType):
        self.name = name
        self.commanders_type = commanders_type
        self.vulnerability = commanders_vulnerability.get(commanders_type, tuple())
        self.AP = 100  # realize for leadership +5%
        self.attack_point = None
        self.defence_point = None
        self.hit_power = 20  # realize for commanders_type
        self.state = State.READY

    def __str__(self):
        return f'Name: {self.name} Type: {self.commanders_type.name}, HP: {self.AP}'

    def next_step_points(self, next_attack: Damage, next_defence: Damage):
        self.attack_point = next_attack
        self.defence_point = next_defence

    def get_hit(self,
                opponent_attack_points: Damage,
                opponent_hit_power: int,
                opponent_type: CommandersType):
        if self.defence_point == opponent_attack_points:
            return'You need to better choose your target! ha ha ha!'
        else:
            self.AP -= opponent_hit_power * (2 if opponent_type in self.vulnerability else 1)
            if self.AP <= 0:
                self.state = State.DEFEATED
                return 'I\'m dead! Game over...'
            else:
                return 'Next round!'


if __name__ == '__main__':
    commander = Commander('Saladin', CommandersType.Cavalry_Support)
    print(commander)
