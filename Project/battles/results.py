from enum import Enum, auto


class GameResult(Enum):
    Win = auto()
    Lose = auto()
    Equal = auto()