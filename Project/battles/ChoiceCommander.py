from Project.battles.CommandersType import CommandersType

ChoiceCommanders = {
    CommandersType.Archer_Skill: {'Herman': 'Hermann.png',
                                  'Yi Seong Gye': 'Yi_Seong_Gye.png',
                                  'Edward': 'Edward.png'},

    CommandersType.Archer_Support: {'Keira': 'Keira.png',
                                    'El Cid': 'El_Cid.png'},

    CommandersType.Archer_Defense: {'Markswoman': 'Markswoman.png',
                                    'Tomyris': 'Tomyris.png'},

    CommandersType.Infantry_Skill: {'Alexander': 'Alexander.png',
                                    'Mundeok': 'Mundeok.png'},

    CommandersType.Infantry_Support: {'Leonidas': 'Leonidas.png',
                                      'Constantine': 'Constantine.png'},

    CommandersType.Infantry_Defence: {'Richard': 'Richard.png',
                                      'Charles Martel': 'Charles_Martel.png'},

    CommandersType.Cavalry_Skill: {'Baibars': 'Baibars.png',
                                   'Genghis Khan': 'Khan.png',
                                   'Attila': 'Attila.png'},

    CommandersType.Cavalry_Support: {'Cao Cao': 'Cao_Cao.png',
                                     'Saladin': 'Saladin.png'},

    CommandersType.Cavalry_Defence: {'Minamoto': 'Minamoto.png',
                                     'Takeda': 'Takeda.png'},

    CommandersType.Leader_Skill: {'Mehmed': 'Mehmed.png',
                                  'Hannibal Barca': 'Hannibal_Barca.png'},

    CommandersType.Leader_Support: {'Aethelflaed': 'Aethelflaed.png',
                                    'Charlesmagne': 'Charlesmagne.png'},

    CommandersType.Leader_Defence: {'Frederick': 'Fredrick.png',
                                    'Wu Zetian': 'Wu.png'}

}
