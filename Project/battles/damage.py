from enum import Enum, auto


class Damage(Enum):
    Healing = auto()
    Defense = auto()
    Attack = auto()

    @classmethod
    def min_value(cls):
        return cls.Healing.value

    @classmethod
    def max_value(cls):
        return cls.Attack.value

    @classmethod
    def has_items(cls, name: str):
        return name in cls._member_names_

