import json
import os

import telebot
from telebot import types
from telebot.types import Message

from Project.battles.damage import Damage
from Project.battles.CommandersType import CommandersType
from Project.battles.ChoiceCommander import ChoiceCommanders
from Project.battles.Commander import Commander
from Project.battles.State import State
from Project.battles.results import GameResult
from Project.battles.commanders_npc import CommanderNPC

bot = telebot.TeleBot(os.getenv('Token_bot'))

state = {}

statistics = {}

stat_file = 'game_stat.json'

damage_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                            one_time_keyboard=True,
                                            row_width=len(Damage))

damage_keyboard.row(*[types.KeyboardButton(dam.name) for dam in Damage])


def update_save_stat(chat_id, result: GameResult):
    print('Statistics update', end='...')
    global statistics

    chat_id = str(chat_id)

    if statistics.get(chat_id, None) is None:
        statistics[chat_id] = {}

    if result == GameResult.Win:
        statistics[chat_id]['Win'] = statistics[chat_id].get('Win', 0) + 1
    elif result == GameResult.Lose:
        statistics[chat_id]['Lose'] = statistics[chat_id].get('Lose', 0) + 1
    elif result == GameResult.Equal:
        statistics[chat_id]['Equal'] = statistics[chat_id].get('Equal', 0) + 1
    else:
        print(f'No result {result}')

    with open(stat_file, 'w') as file:
        json.dump(statistics, file)

    print('Update completed!')


def load_stat():
    print('Download statistics...', end='')
    global statistics

    try:
        with open(stat_file, 'r') as file:
            statistics = json.load(file)
        print('Loading is complete!')
    except FileNotFoundError:
        statistics = {}
        print('File not found!')


@bot.message_handler(commands=['help', 'info'])
def help_command(message):
    bot.send_message(message.chat.id, 'Hello, Polina!\n/start start of the game\n/stat show statistics')


@bot.message_handler(commands=["stat"])
def stat(message):
    global statistics
    if statistics.get(str(message.chat.id), None) is None:
        user_stat = 'No statistics for the game!'
    else:
        user_stat = 'Your results:'
        for res, num in statistics[str(message.chat.id)].items():
            user_stat += f'\n{res}: {num}'

    bot.send_message(message.chat.id, text=user_stat)


@bot.message_handler(commands=['start'])
def start(message):
    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=2)
    yes_no_keyboard.row('Yeah', 'Nope')

    bot.send_message(message.from_user.id,
                     text='<i>Ready for battle?</i>',
                     parse_mode='HTML',
                     reply_markup=yes_no_keyboard)

    bot.register_next_step_handler(message, start_question_handler)


def start_question_handler(message):
    if message.text.lower() == 'yeah':
        bot.send_message(message.from_user.id, '<i>Let\'s start</i>', parse_mode='HTML')

        create_npc(message)

        ask_user_about_commander_type(message)

    elif message.text.lower() == 'nope':
        bot.send_message(message.from_user.id, '<i>Ok, I\'ll wait!</i>', parse_mode='HTML')

    else:
        bot.send_message(message.from_user.id, '<b>I don\'t know such an answer</b>', parse_mode='HTML')


def create_npc(message):
    print(f'Creating NPC for chat = {message.chat.id}')
    global state
    commander_npc = CommanderNPC()
    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['npc_commander'] = commander_npc

    npc_image_filename = ChoiceCommanders[commander_npc.commanders_type][commander_npc.name]
    bot.send_message(message.chat.id, '<b>Enemy:</b>', parse_mode='HTML')
    with open(f'../image_commanders/{npc_image_filename}', 'rb') as file:
        bot.send_sticker(message.chat.id, file)
        bot.send_message(message.chat.id, commander_npc)
    print(f'Character creation complete for chat id = {message.chat.id}')


def ask_user_about_commander_type(message):
    list_com = []
    for commanders_type in CommandersType:
        list_com.append(types.InlineKeyboardButton(text=commanders_type.name,
                                                   callback_data=f'commanders_type_{commanders_type.value}'))
    markup = types.InlineKeyboardMarkup(row_width=3)
    markup.add(*list_com)
    bot.send_message(message.chat.id, '<b>Choose a commander:</b>', parse_mode='HTML', reply_markup=markup)


@bot.callback_query_handler(func=lambda call: 'commanders_type_' in call.data)
def commander_type_handler(call):
    call_data_split = call.data.split('_')
    if len(call_data_split) != 3 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, 'Restarting the session!')
    else:
        commander_type_id = int(call_data_split[2])

        bot.send_message(call.message.chat.id, '<b>Choose a commander:</b>', parse_mode='HTML')

        ask_user_about_commander_by_type(commander_type_id, call.message)


def ask_user_about_commander_by_type(commander_type_id, message):
    commander_type = CommandersType(commander_type_id)
    commander_dict_by_type = ChoiceCommanders.get(commander_type, {})

    for com_name, com_img in commander_dict_by_type.items():
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text=com_name,
                                              callback_data=f'commander_name_{commander_type_id}_{com_name}'))
        with open(f'../image_commanders/{com_img}', 'rb') as file:
            bot.send_sticker(message.chat.id, file, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: 'commander_name_' in call.data)
def commander_name_handler(call):
    call_data_split = call.data.split('_')
    if len(call_data_split) != 4 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, '<b>Restarting the session!</b>', parse_mode='HTML')
    else:
        commander_type_id, com_name = int(call_data_split[2]), call_data_split[3]
        create_user_commander(call.message, commander_type_id, com_name)

        bot.send_message(call.message.chat.id, '<b>Start of the game!</b>', parse_mode='HTML')

        game_next_step(call.message)


def create_user_commander(message, commander_type_id, com_name):
    print(f'Creating of a commander for chat id = {message.chat.id}')
    global state
    user_commander = Commander(name=com_name,
                               commanders_type=CommandersType(commander_type_id))

    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['user_commander'] = user_commander

    image_filename = ChoiceCommanders[user_commander.commanders_type][user_commander.name]
    bot.send_message(message.chat.id, '<b>Your choice:</b>', parse_mode='HTML')
    with open(f'../image_commanders/{image_filename}', 'rb') as file:
        bot.send_sticker(message.chat.id, file)
        bot.send_message(message.chat.id, user_commander)

    print(f'Character creation complete for chat id = {message.chat.id}')


def game_next_step(message: Message):
    bot.send_message(message.chat.id, '<ins>Choose effect for defense:</ins>', parse_mode='HTML',
                     reply_markup=damage_keyboard)

    bot.register_next_step_handler(message, reply_defend)


def reply_defend(message: Message):
    if not Damage.has_items(message.text):
        bot.send_message(message.chat.id, '<b>You need to choose an option on the keyboard!</b>',
                         parse_mode='HTML')
        game_next_step(message)
    else:
        bot.send_message(message.chat.id, '<ins>Choose effect for attack:</ins>', parse_mode='HTML',
                         reply_markup=damage_keyboard)

        bot.register_next_step_handler(message, reply_attack, defend_effect=message.text)


def reply_attack(message: Message, defend_effect: str):
    if not Damage.has_items(message.text):
        bot.send_message(message.chat.id, '<b>You need to choose an option on the keyboard!</b>',
                         parse_mode='HTML')
        game_next_step(message)
    else:
        attack_effect = message.text

        global state

        user_commander = state[message.chat.id]['user_commander']

        commander_npc = state[message.chat.id]['npc_commander']

        user_commander.next_step_points(next_attack=Damage[attack_effect],
                                        next_defence=Damage[defend_effect])
        commander_npc.next_step_points()

        game_step(message, user_commander, commander_npc)


def game_step(message: Message, user_commander: Commander, commander_npc: Commander):
    comment_npc = commander_npc.get_hit(opponent_attack_points=user_commander.attack_point,
                                        opponent_hit_power=user_commander.hit_power,
                                        opponent_type=user_commander.commanders_type)
    bot.send_message(message.chat.id, f'<i>NPC commander:</i> {comment_npc}\n<b>AP:</b> {commander_npc.AP}',
                     parse_mode='HTML')
    comment_user = user_commander.get_hit(opponent_attack_points=commander_npc.attack_point,
                                          opponent_hit_power=commander_npc.hit_power,
                                          opponent_type=commander_npc.commanders_type)
    bot.send_message(message.chat.id, f'<i>Your commander:</i> {comment_user}\n<b>AP:</b> {user_commander.AP}',
                     parse_mode='HTML')

    if commander_npc.state == State.READY and user_commander.state == State.READY:
        bot.send_message(message.chat.id, '<i>We continue the battle!</i>', parse_mode='HTML')
        game_next_step(message)
    elif commander_npc.state == State.DEFEATED and user_commander.state == State.DEFEATED:
        bot.send_message(message.chat.id, 'Equal')
        update_save_stat(message.chat.id, GameResult.Equal)
    elif commander_npc.state == State.DEFEATED:
        bot.send_message(message.chat.id, 'You won!')
        with open('../image_commanders/Win.png', 'rb') as file:
            bot.send_sticker(message.chat.id, file)
        update_save_stat(message.chat.id, GameResult.Win)
    elif user_commander.state == State.DEFEATED:
        bot.send_message(message.chat.id, 'You lose!')
        with open('../image_commanders/game_over_PNG2.png', 'rb') as file:
            bot.send_sticker(message.chat.id, file)
        update_save_stat(message.chat.id, GameResult.Lose)


if __name__ == '__main__':
    load_stat()
    print('Starting the bot')
    bot.polling(none_stop=True, interval=0)
    print('The bot has stopped!')
