from enum import Enum, auto


class CommandersType(Enum):
    """
    More information: https://roc.lilithgames.com
    """
    Archer_Skill = auto()
    Archer_Support = auto()
    Archer_Defense = auto()
    Infantry_Skill = auto()
    Infantry_Support = auto()
    Infantry_Defence = auto()
    Cavalry_Skill = auto()
    Cavalry_Support = auto()
    Cavalry_Defence = auto()
    Leader_Skill = auto()
    Leader_Support = auto()
    Leader_Defence = auto()

    @classmethod
    def min_value(cls):
        return cls.Archer_Skill.value

    @classmethod
    def max_value(cls):
        return cls.Leader_Defence.value
