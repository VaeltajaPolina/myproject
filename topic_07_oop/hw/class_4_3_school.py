"""
Класс School.

Поля:
список людей в школе (общий list для Pupil и Worker): people,
номер школы: number.

Методы:
get_avg_mark: вернуть средний балл всех учеников школы
get_avg_salary: вернуть среднюю зп работников школы
get_worker_count: вернуть сколько всего работников в школе
get_pupil_count: вернуть сколько всего учеников в школе
get_pupil_names: вернуть все имена учеников (с повторами, если есть)
get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
get_max_pupil_age: вернуть возраст самого старшего ученика
get_min_worker_salary: вернуть самую маленькую зп работника
get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
(список из одного или нескольких элементов)
"""

from topic_07_oop.hw.class_4_1_pupil import Pupil
from topic_07_oop.hw.class_4_2_worker import Worker
from statistics import mean


class School:

    def __init__(self, people: list, number: int):
        self.people = people
        self.number = number

    def get_pupil(self):
        return [p for p in self.people if isinstance(p, Pupil)]

    def get_worker(self):
        return [w for w in self.people if isinstance(w, Worker)]

    def get_avg_mark(self):
        val = [m.get_avg_mark() for m in self.get_pupil()]
        new_list = []
        for x in val:
            new_list.append(x)
        return mean(new_list)

    def get_avg_salary(self):
        return mean([s.salary for s in self.people if isinstance(s, Worker)])

    def get_worker_count(self):
        return len(self.get_worker())

    def get_pupil_count(self):
        return len(self.get_pupil())

    def get_pupil_names(self):
        return [np.name for np in self.get_pupil()]

    def get_unique_worker_positions(self):
        return set([p.position for p in self.get_worker()])

    def get_max_pupil_age(self):
        return max([a.age for a in self.get_pupil()])

    def get_min_worker_salary(self):
        return min([s.salary for s in self.get_worker()])

    def get_min_salary_worker_names(self):
        min_sal = self.get_min_worker_salary()
        return [nw.name for nw in self.get_worker() if nw.salary == min_sal]


if __name__ == '__main__':
    pupil_1 = Pupil('Петя',
                    15,
                    {
                        'math': [3, 3, 3],
                        'history': [4, 4, 4],
                        'english': [5, 5, 5]
                    })
    pupil_2 = Pupil('Маша',
                    15,
                    {
                        'math': [4, 4, 4],
                        'history': [4, 4, 4],
                        'english': [4, 4, 4]
                    })

    pupil_3 = Pupil('Маша',
                    13,
                    {
                        'math': [5, 5, 5],
                        'history': [5, 5, 5],
                        'english': [5, 5, 5]
                    })

    worker_1 = Worker('Миша',
                      86324.50,
                      "Учитель математики")

    worker_2 = Worker('Лена',
                      56324.50,
                      "Повар")

    worker_3 = Worker('Василиса',
                      56324.50,
                      "Учитель математики")
    school = School([pupil_1, pupil_2, pupil_3, worker_1, worker_2, worker_3], 333)

    print(school.get_avg_mark())
    print(school.get_avg_salary())
    print(school.get_max_pupil_age())






