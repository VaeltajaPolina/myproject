"""
Класс Pupil.

Поля:
имя: name,
возраст: age,
dict с оценками: marks (пример: {'math': [3, 5], 'english': [5, 5, 4] ...].

Методы:
get_all_marks: получить список всех оценок,
get_avg_mark_by_subject: получить средний балл по предмету (если предмета не существует, то вернуть 0),
get_avg_mark: получить средний балл (все предметы),
__le__: вернуть результат сравнения (<=) средних баллов (все предметы) двух учеников.
"""
from statistics import mean


class Pupil:

    def __init__(self, name: str, age: int, marks: dict):
        self.name = name
        self.age = age
        self.marks = marks

    def get_all_marks(self):
        val = self.marks.values()
        new_list = []
        for x in val:
            new_list.extend(x)
        return new_list

    def get_avg_mark(self):
        val = self.marks.values()
        new_list = []
        for x in val:
            new_list.extend(x)
        return mean(new_list)

    def get_avg_mark_by_subject(self, key):
        if key in self.marks.keys():
            new_list = self.marks.get(key, ())
            return mean(new_list)
        else:
            return 0

    def __le__(self, other):
        val = self.marks.values()
        new_list = []
        for x in val:
            new_list.extend(x)
        val2 = other.marks.values()
        new_list2 = []
        for x in val2:
            new_list2.extend(x)
        return mean(new_list) <= mean(new_list2)


if __name__ == '__main__':
    H_pupil1 = Pupil('HarryPotter', 12, {'Potions': [4, 3, 3, 3], 'Brooms': [5, 5, 5], 'Charms': [4, 5, 4, 4]})
    H_pupil2 = Pupil('HermioneGranger', 12, {'Potions': [5, 4, 5, 5], 'Brooms': [4, 4, 5, 5], 'Charms': [5, 5, 5, 5]})
    print((H_pupil1.get_all_marks()))
    print(H_pupil1.get_avg_mark())
    print(H_pupil1.get_avg_mark_by_subject('Brooms'))
    print(H_pupil1 <= H_pupil2)
