"""
Класс Goat.

Поля:
имя: name,
возраст: age,
сколько молока дает в день: milk_per_day.

Методы:
get_sound: вернуть строку 'Бе-бе-бе',
__invert__: реверс строки с именем (например, была Маруся, а стала Ясурам). вернуть self
__mul__: умножить milk_per_day на число. вернуть self
"""


class Goat:

    def __init__(self, name: str, age: int, milk_per_day: int):
        self.name = name
        self.age = age
        self.milk_per_day = milk_per_day

    def get_sound(self):
        return 'Бе-бе-бе'

    def __invert__(self):
        self.name = self.name[::-1].title()
        return self

    def __mul__(self, other):
        self.milk_per_day = self.milk_per_day * other
        return self


if __name__ == '__main__':
    G1 = Goat('HarryPotter', 1, 0)
    G2 = Goat('HermioneGrander', 2, 5)
    G3 = Goat('DracoMalfoy', 6, 20)
    print(type(~G3))
    print(G2.get_sound())
    print(G2 * 5)

