"""
Класс Worker.

Поля:
имя: name,
зарплата: salary,
должность: position.

Методы:
__gt__: возвращает результат сравнения (>) зарплат работников.
__len__: возвращает количетсво букв в названии должности.
"""


class Worker:

    def __init__(self, name: str, salary, position: str):
        self.name = name
        self.salary = salary
        self.position = position

    def __gt__(self, other):
        return self.salary > other.salary

    def __len__(self):
        return len(self.position)


if __name__ == '__main__':
    H_worker = Worker('Mr Dursley', 4000, 'businessman')
    H_worker2 = Worker('Albus Dumbledore', 2900, 'Headmaster')
    print(H_worker > H_worker2)
    print(len(H_worker2))
