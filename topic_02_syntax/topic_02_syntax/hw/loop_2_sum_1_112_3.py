"""
Функция sum_1_112_3.

Вернуть сумму 1+4+7+10+...109+112.
"""
def sum_1_112_3():
    return sum(range(1, 113, 3))


if __name__ == '__main__':
    print(sum_1_112_3())
