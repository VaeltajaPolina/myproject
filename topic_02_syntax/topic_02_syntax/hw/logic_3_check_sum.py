"""
Функция check_sum.

Принимает 3 числа.
Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
"""
def check_sum(x, y, z):
    return (x + y) == z or (x + z) == y or (z + y) == x

if __name__ == '__main__':
    print(check_sum())

