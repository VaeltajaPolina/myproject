"""
Функция print_symbols_if.

Принимает строку.

Если строка нулевой длины, то вывести строку "Empty string!".

Если длина строки больше 5, то вывести первые три символа и последние три символа.
Пример: string='123456789' => result='123789'

Иначе вывести первый символ столько раз, какова длина строки.
Пример: string='345' => result='333'
"""
def print_symbols_if(x):
    z = str(x)
    n = len(z)
    if n == 0:
        print('Empty string!')
    elif n > 5:
        print(z[:3] + z[-3:])
    else:
        print(z[0] * n)


if __name__ == '__main__':
    print(print_symbols_if())




