"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
Если число меньше или равно 0, то вернуть "Must be > 0!".
"""

def count_odd_num(x):
    n = 0
    if type(x) != int:
        return 'Must be int!'
    if x <= 0:
        return 'Must be > 0!'
    else:
        while x > 0:
            if x % 2 != 0:
                n += 1
            x //= 10
        return n


if __name__ == '__main__':
    print(count_odd_num(85741))
