"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""
def check_substr (z, n):
    if len(z) == len(n):
        return False
    elif len(n) > len(z):
        return z in n
    elif len(z) > len(n):
        return n in z
    else:
        return False


if __name__ == '__main__':
    print(check_substr('vaeltaja', 'vae'))
