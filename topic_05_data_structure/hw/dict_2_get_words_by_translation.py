"""
Функция get_words_by_translation.

Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (eng).

Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
если нет, то "Can't find English word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""


def get_words_by_translation(ru_eng: dict, eng: str):
    if type(ru_eng) != dict:
        return "Dictionary must be dict!"
    elif len(ru_eng) == 0:
        return "Dictionary is empty!"
    elif type(eng) != str:
        return "Word must be str!"
    elif len(eng) == 0:
        return "Word is empty!"
    else:
        new_ru_eng = [key for key, value in ru_eng.items() if eng in value]
        if len(new_ru_eng) == 0:
            return f"Can't find English word: {eng}"
        else:
            return new_ru_eng


if __name__ == '__main__':
    test_dict = get_words_by_translation({'принимать': ['adopt', 'accept'],
                                          'заграница': 'abroad', 'украшать': 'adorn'}, 'accept')
    print(test_dict)
