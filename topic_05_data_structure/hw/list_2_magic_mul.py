"""
Функция magic_mul.

Принимает 1 аргумент: список my_list.

Возвращает список, который состоит из
[первого элемента my_list]
+ [три раза повторенных списков my_list]
+ [последнего элемента my_list].

Пример:
входной список [1,  'aa', 99]
результат [1, 1,  'aa', 99, 1,  'aa', 99, 1,  'aa', 99, 99].

Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
Если список пуст, то возвращать строку 'Empty list!'.
"""

def magic_mul(my_list:list):
    if type(my_list) != list:
        return 'Must be list!'
    elif len(my_list) == 0:
        return 'Empty list!'
    new_my_list = []
    A = my_list[0]
    B = my_list * 3
    C = my_list[-1]
    new_my_list.append(A)  # or new_my_list.extend(A) if A = my_list[:1]
    new_my_list.extend(B)
    new_my_list.append(C)
    return new_my_list

if __name__ == '__main__':
    print(magic_mul([1]))
