"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список: [
список ключей,
список значений,
количество уникальных элементов в списке ключей,
количество уникальных элементов в списке значений
].

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

Если dict пуст, то возвращать ([], [], 0, 0).
"""


def dict_to_list(my_dict: dict):
    if type(my_dict) != dict:
        return 'Must be dict!'
    if len(my_dict) == 0:
        return [], [], 0, 0
    keys_list = list(my_dict.keys())
    values_list = list(my_dict.values())
    unique_values = len(set(values_list))
    unique_keys = len(keys_list)  #ключи в словаре уникальны, можно без set
    return keys_list, values_list, unique_keys, unique_values


if __name__ == '__main__':
    print(dict_to_list({1: 'lolo', 2: 34, 3: 34, 4: 'pupu'}))
