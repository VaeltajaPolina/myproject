"""
Функция pow_start_stop.

Принимает числа start, stop.

Возвращает список состоящий из квадратов значений от start до stop (не включая).

Пример: start=3, stop=6, результат [9, 16, 25].

Если start или stop не являются int, то вернуть строку 'Start and Stop must be int!'.
"""


def pow_start_stop(start, stop):
    if type(start) != int:
        return 'Start and Stop must be int!'
    elif type(stop) != int:
        return 'Start and Stop must be int!'
    # result = []
    # for x in range(start, stop, 1):
    #     result.append((x ** 2))
    result = [x**2 for x in range(start, stop, 1)]
    return result


if __name__ == '__main__':
    print(pow_start_stop(2, 4))
