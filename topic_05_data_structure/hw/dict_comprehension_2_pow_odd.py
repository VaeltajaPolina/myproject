"""
Функция pow_odd.

Принимает число n.

Возвращает словарь, в котором
ключ - это нечетное число в диапазоне от 0 до n (включая)
значение - квадрат ключа.

Пример: n = 5, четные числа [1, 3, 5], результат {1: 1, 3: 9, 5: 25}.

Если n не является int, то вернуть строку 'Must be int!'.
"""


def pow_odd(n: int):
    if type(n) != int:
        return 'Must be int!'
    else:
        new_dict = {k: k ** 2 for k in range(0, n + 1) if k % 2 != 0}
        return new_dict


if __name__ == '__main__':
    print(pow_odd(5))
