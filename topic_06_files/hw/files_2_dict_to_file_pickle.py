"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""

import pickle


def save_dict_to_file_pickle(my_way, my_dict):
    with open(my_way, 'wb') as pickle_file:
        pickle.dump(my_dict, pickle_file)


if __name__ == '__main__':
    new_dict = {'a': 1, 'b': 2, 'c': 3}
    way = 'dict.pkl'
    save_dict_to_file_pickle(way, new_dict)

    with open(way, 'rb') as f:
        dict_loaded = pickle.load(f)
    print(f'type: {type(new_dict)}')
    print(f'equal: {new_dict == dict_loaded}')
