"""
Функция save_dict_to_file_classic.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Проверить, что записалось в файл.
"""


def save_dict_to_file_classic(my_way, my_dict):
    with open(my_way, 'w') as my_file:
        my_file.write(str(my_dict))


if __name__ == '__main__':
    path = 'my_dict.txt'
    save_dict_to_file_classic(path, {1: 'lolo', 2: {1, 2, 66}, 3: 22})

    with open(path, 'r') as f:
        check = f.read()
        print(check)
