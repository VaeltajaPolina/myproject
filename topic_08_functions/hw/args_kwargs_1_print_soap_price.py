"""
Функция print_soap_price.

Принимает 2 аргумента: название мыла (строка) и неопределенное количество цен (*args).

Функция print_soap_price выводит вначале название мыла, а потом все цены на него.

В функции main вызывается функция print_soap_price и передается название мяла и произвольное количество цен.

Пример: print_soap_price('Dove’, 10, 50) или print_soap_price('Мылко’, 456, 876, 555).
"""


def print_soap_price(name_soap: str, *price):
    print(f'\'{name_soap}\'', *price)


if __name__ == '__main__':

    print_soap_price('Rosa', 50, 45, 93.32)
    print_soap_price('Dove', (34, 65, 44, 88))
    print_soap_price('Duru', [22, 44, 31])
    print_soap_price('Lilia', {99, 33, 12})

