"""
Функция flower_with_default_vals.

Принимает 3 аргумента:
цветок (по умолчанию "ромашка"),
цвет (по умолчанию "белый"),
цена (по умолчанию 10.25).

Функция flower_with_default_vals выводит строку в формате
"Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>".

При этом в функции flower_with_default_vals есть проверка на то, что цветок и цвет - это строки

(* Подсказка: type(x) == str или isinstance(s, str)), а также цена - это число больше 0, но меньше 10000.
В функции main вызвать функцию flower_with_default_vals различными способами
(перебрать варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена).

(* Использовать именованные аргументы).
"""


def flower_with_default_vals(**kwargs):
    for flower, param in kwargs.items():
        if type(flower) != str or type(param[0]) != str or type(param[1]) != int or not 0 < param[1] < 10000:
            return False
        print(f'Flower: {flower}| Color: {param[0]}| Price: {param[1]}')


if __name__ == '__main__':

    flower_with_default_vals(Tulip=('red', 4), Rose='yellow', Lilia=4, red=3, Primrose=('pink', 3))


